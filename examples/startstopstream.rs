use std::env;
use anyhow::Result;
//use obws::Client;



#[tokio::main]
async fn main() -> Result<()> {

    // read .env file
    dotenv::dotenv().expect(".env file not found");

    // get args
    let args: Vec<String> = env::args().collect();
    println!("{:#?}", args);
    let mut query = "";
    if args.len() > 1 {
        query = &args[1];
        println!("{query}");
    }
    if query == "status" {
        println!("demande de statut");
    }
/*     // Connect to the OBS instance through obs-websocket.
    let client = Client::connect("127.0.0.1", 4444).await?;
    client.login(env::var("OBS_PASSWORD").ok()).await?;


    let stream_status = client.virtual_cam().start_stop_streaming().await?;
    println!("{:#?}", stream_status);
 */
    Ok(())
}