use std::env;
use anyhow::Result;
use obws::Client;



#[tokio::main]
async fn main() -> Result<()> {

    dotenv::dotenv().expect(".env file not found"); // read .env file

    // Connect to the OBS instance through obs-websocket.
    let client = Client::connect("127.0.0.1", 4444).await?;
    client.login(env::var("OBS_PASSWORD").ok()).await?;


    let stream_status = client.virtual_cam().start_stop_virtual_cam().await?;
    println!("{:#?}", stream_status);

    Ok(())
}