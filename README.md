# OBS Remote Control

Control your OBS stream remotely as a director and with co-directors.

>To make it work, create a `.env` file in the root directory of the project and add a line with `OBS_PASSWORD = yourobswebsocketpassword`

## State of the project

This is actually an exploration of the OBS Websocket API. Everything is run from `cargo`.

The following is done :

- API version
- Virtual Cam start/stop
- List of the scenes
- Screenshots of the scenes

Next steps :

- Switch to a scene
- Start/stop streaming
- Enable/Disable a source in a scene
- Get/Set text in a Text source
- Start/Stop a Video source

### Examples

``` 
    cargo run                       // will run src/main.rs
    cargo run --example virtualcam  // will run examples/virtualcam.rs
    cargo run --example simple      // will run examples/simple.rs
```

## Access your OBS from outside your network

In order to control your OBS instance from the outside, you'll need to install a reverse SSH proxy on a server and tunnel into it. *(this will be covered later)*

## CLI? Standalone with GUI? Web based frontend?

Whatever solution that will appear in the course of the developement, a CLI should always be available, at least for testing purpose.

I still have to figure out what UI framework will be easy and flexible enough to minimize that part. As long as we're in exploration mode, an *Immediate Mode* framework like [Dear ImGUI](https://imgui-test.readthedocs.io/en/latest/#) might be the way to go.

This project is also a good opportunity to test some new things like [WebAssembly](https://webassembly.org/) ... we'll see.

## Integration of VDO.ninja

(To Do)