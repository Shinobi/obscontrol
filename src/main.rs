use std::env;
use anyhow::Result;
use obws::{requests::SourceScreenshot, Client};
use tokio::fs;

// Async screenshot of Source

async fn source_screenshot(obs: &obws::Client, source_name: &str, img_type: &str) -> Result<()> {
    let screenshot = obs
        .sources()
        .take_source_screenshot(SourceScreenshot {
            source_name: Some(source_name),
            embed_picture_format: Some(img_type),
            ..Default::default()
        })
        .await?;
    let image = screenshot.img.unwrap();
    let pos = image.find("base64,").unwrap();
    let image = base64::decode(&image[pos + 7..])?;
    
    let mut image_filename: String = source_name.to_string();
    image_filename.push_str(".");
    image_filename.push_str(img_type);
    fs::write(image_filename, &image).await?;
    Ok(())
}


#[tokio::main]
async fn main() -> Result<()> {

    dotenv::dotenv().expect(".env file not found"); // read .env file

    // Connect to the OBS instance through obs-websocket.
    let client = Client::connect("127.0.0.1", 4444).await?;
    client.login(env::var("OBS_PASSWORD").ok()).await?;

    // Get and print out version information of OBS and obs-websocket.
    //let version = client.general().get_version().await?;
    //println!("{:#?}", version);

    // Get a list of available scenes and print them out.
    println!("Liste des scénes :");
    let scene_list = client.scenes().get_scene_list().await?;
    for scene in scene_list.scenes {
        let mut pref = "";
        if scene_list.current_scene == scene.name { pref = ">";}
        println!("{pref}{:#?}",scene.name);
        //get a screenshot of the scene
        source_screenshot(&client, &scene.name, "png").await?;
    }
    //println!("{:#?}", scene_list);
    //let stream_status = client.streaming().get_streaming_status().await?;
    //println!("{:#?}", stream_status);

/*     let stream_status = client.virtual_cam().start_stop_virtual_cam().await?;
    println!("{:#?}", stream_status);
 */
    Ok(())
}